// Test the persistence service
/* eslint-env mocha */

const debug = require('debug')('yabaas:persistence-test')  // eslint-disable-line

const chakram = require('chakram')
const expect = chakram.expect

const config = require('config')
const apiUrl = 'http://' + config.get('api.host') + ':' + config.get('api.port')

describe('Persistence API Test Suite:', function () {
  let id = ''

  it('POST Method', function () {
    const response = chakram.post(apiUrl + '/persistence/route', {field: 'value'})
    expect(response).to.have.status(201)
    return chakram.wait()
  })

  it('GET Method', function () {
    const response = chakram.get(apiUrl + '/persistence/route')
    expect(response).to.have.status(200)
    expect(response).to.have.json(json => {
      // debug(json)
      expect(json).to.have.length(1)
      expect(json[0].field).to.equal('value')
      id = json[0]._id
    })
    return chakram.wait()
  })

  it('GET Method by id', function () {
    const response = chakram.get(apiUrl + '/persistence/route/' + id)
    expect(response).to.have.status(200)
    expect(response).to.have.json(json => {
      // debug(json)
      expect(json.field).to.equal('value')
    })
    return chakram.wait()
  })
})
