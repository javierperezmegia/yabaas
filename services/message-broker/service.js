// Advanced Message Queuing Protocol using RabbitMQ service
//

const debug = require('debug')('yabaas:message-broker')  // eslint-disable-line

const express = require('express')
const router = express.Router()

const message = require('./module')

router.post('/', function (req, res, next) {
  debug('POST /message ')
  res.status(200).json({message: 'post()'})
})

router.post('/publish', function (req, res, next) {
  debug('POST /message/publish ' + JSON.stringify(req.body))
  message.publish(req.body.queue, req.body.message)
  res.status(200).json({message: 'post()'})
})

router.get('/', function (req, res, next) {
  debug('GET /message')
  res.status(200).json({message: 'get()'})
})

router.get('/subscribe/:queueId', function (req, res, next) {
  debug('GET /message/subscribe/' + req.params.queueId)
  message.subscribe(req.params.queueId, (response) => {
    debug('GET /message/subscribe/' + req.params.queueId + ' message=' + response)
    res.status(200).json({queue: req.params.queueId, message: response})
  })
})

module.exports = router
