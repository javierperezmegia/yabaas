// Test the validation service
/* eslint-env mocha */

const debug = require('debug')('yabaas:validation-test')  // eslint-disable-line

const chakram = require('chakram')
const expect = chakram.expect

const config = require('config')
const apiUrl = 'http://' + config.get('api.host') + ':' + config.get('api.port')

describe('Validation API Test Suite:', function () {
  it('Rule: unique: First post should persist the field value', function () {
    const response = chakram.post(apiUrl + '/persistence/dummy_route', {dummy_field: 'dummy_value', another_dummy_field: 'dummy_value'})
    expect(response).to.have.status(201)
    return chakram.wait()
  })

  it('Rule: unique: Posting again same field with same value should not persist the field value', function () {
    const response = chakram.post(apiUrl + '/persistence/dummy_route', {dummy_field: 'dummy_value', another_dummy_field: 'dummy_value'})
    expect(response).to.have.status(409)
    return chakram.wait()
  })

  it('Rule: required: Posting without a required field should return an error', function () {
    const response = chakram.post(apiUrl + '/persistence/dummy_route', {another_dummy_field: 'another_dummy_value'})
    expect(response).to.have.status(409)
    return chakram.wait()
  })
})
