// Test the home end point
/* eslint-env mocha */

const debug = require('debug')('yabaas:home-test')  // eslint-disable-line

const chakram = require('chakram')
const expect = chakram.expect

const config = require('config')
const apiUrl = 'http://' + config.get('api.host') + ':' + config.get('api.port')

describe('Home API Test Suite:', () => {
  let response

  before(() => {
    response = chakram.get(apiUrl + '/')
    return response
  })

  it('should return 200 on success', () => {
    return expect(response).to.have.status(200)
  })

  it('should return content type header', () => {
    return expect(response).to.have.header('content-type', 'application/json; charset=utf-8')
  })

  it('should return CORS headers', () => {
    expect(response).to.have.header('Access-Control-Allow-Origin', '*')
    expect(response).to.have.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    return chakram.wait()
  })

  it('should include title and version', () => {
    return expect(response).to.have.schema('api', { 'required': [
      'title',
      'version'
    ]})
  })

  it('should return the API title', () => {
    return expect(response).to.have.json('api.title', 'Backend as a Service')
  })

  it('should return a valid semantic version number', () => {
    return expect(response).to.have.json((json) => {
      const version = json.api.version
      return expect(/^(?:(\d+)\.)?(?:(\d+)\.)?(\*|\d+)$/.test(version)).to.be.true
    })
  })
})
