const jsonHome = require(process.cwd() + '/json-home')

var express = require('express')
var router = express.Router()

/* GET home page. */
router.get('/', function (req, res, next) {
  res.status(200).send(jsonHome)
})

module.exports = router
