// List of allowed public routes

const jsonAuthentication = {
  api: {
    authentication: {
      equal_to: [
        '/'
      ],
      start_by: [
        '/authentication/register',
        '/authentication/sign_in',
        '/persistence',
        '/email',
        '/message'
      ]
    }
  }
}

module.exports = jsonAuthentication
