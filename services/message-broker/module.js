
// Advanced Message Queuing Protocol using RabbitMQ module
//

const debug = require('debug')('yabaas:message-broker')  // eslint-disable-line
const config = require('config')
const amqp = require('amqplib/callback_api')

/**
 * Connect to message broker
 */

const url = 'amqp://' + config.get('rabbit.user') + ':' + config.get('rabbit.pass') + '@' + config.get('rabbit.host')

const delay = 1000 * 2  // 2 seconds

let broker = ''

const connect = function () {
  amqp.connect(url, (error, connection) => {
    if (error) return debug(error + ' ' + url)
    clearInterval(timer)
    broker = connection
    debug('Rabbit connected on ' + url)
  })
}

const timer = setInterval(connect, delay)

/**
 * POST Method received
 */

exports.publish = function (queue, message) {
  debug('publish() queue=[' + queue + '] message=[' + message + ']')

  broker.createChannel((error, channel) => {
    if (error) return debug(error)
    channel.assertQueue(queue, {durable: false})
    channel.sendToQueue(queue, Buffer.from(message))
    debug('publish() queue=[' + queue + '] message=[' + message + '] OK')
  })
}

/**
 * GET Method received
 */

exports.subscribe = function (queue, callback) {
  debug('subscribe() queue=[' + queue + ']')

  broker.createChannel((error, channel) => {
    if (error) return debug(error)
    channel.assertQueue(queue, {durable: false})
    channel.consume(queue, (message) => {
      debug('subscribe() queue=[' + queue + '] message=[' + message.content.toString() + '] OK')
      callback(message.content.toString())
    }, {noAck: true})
  })
}
