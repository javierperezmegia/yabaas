const _ = require('lodash')

let users = []

exports.exists = function (identifier) {
  return users.filter(item => item.email === identifier).length !== 0
}

exports.create = function (identifier) {
  users.push({ email: identifier })
}

exports.find = function (identifier) {
  return users[_.findIndex(users, {email: identifier})]
}
