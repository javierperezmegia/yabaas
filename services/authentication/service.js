// User authentication using JWT (JSON Web Token)
//

const debug = require('debug')('yabaas:authentication')

const express = require('express')
const router = express.Router()

const authentication = require('./module')

router.post('/register', function (req, res, next) {
  const identifier = req.body.email
  debug('POST /authentication/register ' + identifier)

  if (identifier === '') {
    return res.status(409).send({message: 'Register failed. Error: Empty email.'})
  }

  authentication.register(identifier)
    .then((result) => { res.status(201).send({message: 'Register success.', token: result}) })
    .catch((reason) => { res.status(409).send({message: 'Register failed. ' + reason}) })
})

router.post('/sign_in', function (req, res, next) {
  const identifier = req.body.email
  debug('POST /authentication/sign_in ' + identifier)

  if (identifier === '') {
    return res.status(409).send({message: 'Sign in failed. Error: Empty email.'})
  }

  authentication.signIn(identifier)
    .then((result) => { res.status(200).send({message: 'Sign in success.', token: result}) })
    .catch((reason) => { res.status(401).send({message: 'Sign in failed. ' + reason}) })
})

// dummy end point to test jwt authentication
router.get('/private', function (req, res, next) {
  res.json({message: 'You can see this with a token.'})
})

module.exports = router
