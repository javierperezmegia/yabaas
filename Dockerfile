FROM node:8.5.0

WORKDIR /usr/src/app

COPY package.json .
COPY package-lock.json .

RUN npm install --silent --ignore-scripts --progress=false --unsafe-perm
