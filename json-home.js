const config = require(process.cwd() + '/package.json')

const jsonHome = {
  api: {
    title: 'Backend as a Service',
    version: config.version
  }
}

module.exports = jsonHome
