const debug = require('debug')('yabaas:app')
var express = require('express')
var path = require('path')
var logger = require('morgan')
var cookieParser = require('cookie-parser')
var bodyParser = require('body-parser')
const multer = require('multer')

var index = require('./routes/index')
const persistence = require('./services/persistence/service')
const validation = require('./services/validation/service')
const authentication = require('./services/authentication/service')
const requireLogin = require('./services/authentication/passport').requireLogin
const search = require('./services/search/service')
// const message = require('./services/message-broker/service')
// const email = require('./services/email/service')

var app = express()

app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

// https://enable-cors.org/server_expressjs.html
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
  next()
})

app.use(function (req, res, next) {
  if (req.is('multipart/form-data')) {
    debug('multipart form-data request')
    app.use(multer().none())
  }
  next()
})

app.use(requireLogin)
app.use('/', index)
app.use('/persistence', persistence)
app.use('/validation', validation)
app.use('/authentication', authentication)
app.use('/search', search)
// app.use('/message', message)
// app.use('/email', email)

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found')
  err.status = 404
  next(err)
})

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.json({message: 'error: ' + err.message})
})

module.exports = app
