// Persistence API End Point
//

const debug = require('debug')('yabaas:persistence')

const express = require('express')
const router = express.Router()

const persistence = require('./module')

router.get('/:collectionId', function (req, res, next) {
  const index = req.params.collectionId
  debug('GET /persistence/' + index)

  persistence.read(index)
    .then((results) => {
      debug('200 OK ' + JSON.stringify(results))
      res.status(200).send(results)
    })
    .catch((reason) => {
      debug('409 ERROR ' + JSON.stringify(reason))
      res.status(409).json({message: reason})
    })
})

router.get('/:collectionId/:resourceId', function (req, res, next) {
  const index = req.params.collectionId
  const id = req.params.resourceId
  debug('GET /persistence/' + index + '/' + id)

  persistence.readOne(index, id)
    .then((results) => {
      debug('200 OK ' + JSON.stringify(results))
      res.status(200).send(results)
    })
    .catch((reason) => {
      debug('409 ERROR ' + JSON.stringify(reason))
      res.status(409).json({message: reason})
    })
})

router.post('/:collectionId', function (req, res, next) {
  const index = req.params.collectionId
  const data = req.body
  debug('POST /persistence/' + index + ' data=[' + JSON.stringify(data) + ']')

  persistence.write(index, data)
    .then((results) => {
      debug('201 OK ' + JSON.stringify(results))
      res.status(201).json({message: 'Created.'})
    })
    .catch((reason) => {
      debug('409 ERROR ' + JSON.stringify(reason))
      res.status(409).json({message: reason})
    })
})

module.exports = router
