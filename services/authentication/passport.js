// Authentication JWT passport
//

const debug = require('debug')('yabaas:authentication')  // eslint-disable-line

const express = require('express')
const passport = require('passport')
// const _ = require('lodash')
const jwt = require('jsonwebtoken')
const passportJWT = require('passport-jwt')

const users = require('./user')

const secretOrKey = 'api_secret_key'

const jwtOptions = {}
jwtOptions.secretOrKey = secretOrKey
jwtOptions.jwtFromRequest = passportJWT.ExtractJwt.fromAuthHeaderAsBearerToken()

const jwtStrategy = new passportJWT.Strategy(jwtOptions, function (jwtPayload, next) {
  const user = users.find(jwtPayload.email)

  // next(null, user.exist(jwtPayload.email))
  // if (null, user.exist(jwtPayload.email)) {
  if (user) {
    next(null, user)
  } else {
    next(null, false)
  }
})

passport.use(jwtStrategy)

express().use(passport.initialize())

const jsonAuthentication = require(process.cwd() + '/json-authentication')

exports.requireLogin = function (req, res, next) {
  // debug('requireLogin() ' + req.originalUrl)
  let needed = true

  jsonAuthentication.api.authentication.equal_to.forEach((item, i) => {
    if (req.originalUrl === item) {
      // debug('requireLogin() No authentication needed in ' + req.originalUrl)
      needed = false
    }
  })

  jsonAuthentication.api.authentication.start_by.forEach((query) => {
    if (new RegExp('^' + query, 'i').test(req.originalUrl)) {
      // debug('requireLogin() No authentication needed in ' + req.originalUrl)
      needed = false
    }
  })

  if (!needed) return next()

  passport.authenticate('jwt', { session: false }, (err, user, info) => {
    if (err) return next(err)

    // debug('requireLogin() ' + req.originalUrl + ' user=' + user + ' info=' + info)

    if (!user) {
      return res.status(401).json('Unauthorized')
    } else {
      return next()
    }
  })(req, res, next)
}

exports.getToken = function (identifier) {
  return jwt.sign({ email: identifier }, secretOrKey)
}
