const jsonValidations = {
  api: {
    validations: {
      dummy_route: {
        dummy_field: [ 'unique', 'required' ]
      }
    }
  }
}

module.exports = jsonValidations
