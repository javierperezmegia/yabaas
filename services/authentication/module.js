// Authentication Module
//

const debug = require('debug')('yabaas:authentication')  // eslint-disable-line

const passport = require('./passport')

const user = require('./user')

exports.register = function (identifier) {
  debug('register() identifier=[' + identifier + ']')

  return new Promise((resolve, reject) => {
    if (user.exists(identifier)) {
      reject(new Error('User already exists.'))
    }

    user.create(identifier)
    resolve(passport.getToken(identifier))
  })
}

exports.signIn = function (identifier) {
  debug('signIn() identifier=[' + identifier + ']')

  return new Promise((resolve, reject) => {
    if (!user.exists(identifier)) {
      reject(new Error('User not found.'))
    }

    resolve(passport.getToken(identifier))
  })
}
