// Persistence API End Point
//

const debug = require('debug')('yabaas:search')

const express = require('express')
const router = express.Router()

router.get('/:collectionId', function (req, res, next) {
  debug('GET /search/' + req.params.collectionId)

  const db = req.app.get('db')
  const index = req.params.collectionId
  const query = req.body

  db.collection(index).findOne(query, (err, results) => {
    if (err) {
      debug(err)
      return res.status(409).json({message: 'Find error.'})
    }
    debug('200 OK ' + JSON.stringify(results))
    res.status(200).send(results)
  })
})

module.exports = router
