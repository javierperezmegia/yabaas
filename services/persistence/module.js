// Persistence Module
//

const debug = require('debug')('yabaas:persistence')
const config = require('config')

const mongo = require('mongodb').MongoClient
const ObjectId = require('mongodb').ObjectID

const isValid = require('../validation/module').isValid
const filter = require('../filter/module').filter

/**
 * Connect to database
 */

const mongourl = 'mongodb://' + config.get('mongo.host') + ':' + config.get('mongo.port')

let db

mongo.connect(mongourl, (error, database) => {
  if (error) return debug(error)
  db = database
  debug('Mongo connected on ' + mongourl)
})

/**
 * Module exports
 */

exports.write = function (index, data) {
  debug('write() index=[' + index + '] data=[' + JSON.stringify(data) + ']')

  return new Promise((resolve, reject) => {
    if (!isValid(db, index, data)) {
      reject(new Error('Rule break.'))
    }
    db.collection(index).insertOne(data, (err, result) => {
      if (err) reject(err)
      resolve(result)
    })
  })
}

exports.read = function (index) {
  debug('read() index=[' + index + ']')

  return new Promise((resolve, reject) => {
    const filterCollection = filter(index)

    db.collection(index).find(filterCollection).toArray((error, results) => {
      if (error) reject(error)
      resolve(results)
    })
  })
}

exports.readOne = function (index, id, error, success) {
  debug('readOne() index=[' + index + '] id=[' + id + ']')

  return new Promise((resolve, reject) => {
    const query = {'_id': new ObjectId(id)}

    db.collection(index).findOne(query, (err, results) => {
      if (err) reject(err)
      resolve(results)
    })
  })
}
