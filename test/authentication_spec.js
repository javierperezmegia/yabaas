// Test the token based authentication service
/* eslint-env mocha */

const debug = require('debug')('yabaas:filter-test') // eslint-disable-line

const chakram = require('chakram')
const expect = chakram.expect

const config = require('config')
const apiUrl = 'http://' + config.get('api.host') + ':' + config.get('api.port')

describe('Authentication API Test Suite:', function () {
  let token = ''

  it('Register new user', function () {
    const response = chakram.post(apiUrl + '/authentication/register', {email: 'user@example.com'})
    expect(response).to.have.status(201)
    expect(response).to.have.json('message', 'Register success.')
    expect(response).to.have.json(res => {
      token = res.token
    })
    return chakram.wait()
  })

  it('Email should be unique', function () {
    const response = chakram.post(apiUrl + '/authentication/register', {email: 'user@example.com'})
    expect(response).to.have.status(409)
    expect(response).to.have.json('message', 'Register failed. Error: User already exists.')
    return chakram.wait()
  })

  it('Email should not be empty', function () {
    const response = chakram.post(apiUrl + '/authentication/register', {email: ''})
    expect(response).to.have.status(409)
    expect(response).to.have.json('message', 'Register failed. Error: Empty email.')
    return chakram.wait()
  })

  it('A new user could be registered', function () {
    const response = chakram.post(apiUrl + '/authentication/register', {email: 'new_user@example.com'})
    expect(response).to.have.status(201)
    expect(response).to.have.json('message', 'Register success.')
    expect(response).to.have.json(res => {
      expect(token).not.to.be.equal(res.token)
    })
    return chakram.wait()
  })

  it('Valid email could sign in', function () {
    const response = chakram.post(apiUrl + '/authentication/sign_in', {email: 'user@example.com'})
    expect(response).to.have.status(200)
    expect(response).to.have.json('message', 'Sign in success.')
    expect(response).to.have.json(res => {
      expect(token).to.be.equal(res.token)
    })
    return chakram.wait()
  })

  it('Invalid email could not sign in', function () {
    const response = chakram.post(apiUrl + '/authentication/sign_in', {email: 'bad_user@example.com'})
    expect(response).to.have.status(401)
    expect(response).to.have.json('message', 'Sign in failed. Error: User not found.')
    return chakram.wait()
  })

  it('Email should not be empty', function () {
    const response = chakram.post(apiUrl + '/authentication/sign_in', {email: ''})
    expect(response).to.have.status(409)
    expect(response).to.have.json('message', 'Sign in failed. Error: Empty email.')
    return chakram.wait()
  })
  it('Should get an error if JWT token is not provided', function () {
    const response = chakram.get(apiUrl + '/authentication/private')
    expect(response).to.have.status(401)
    expect(response).to.have.json('Unauthorized')
    return chakram.wait()
  })

  it('Should get an error if JWT token is provided but it is not valid', function () {
    const response = chakram.get(apiUrl + '/authentication/private', {headers: {'Authorization': 'Bearer bad_token'}})
    expect(response).to.have.status(401)
    expect(response).to.have.json('Unauthorized')
    return chakram.wait()
  })

  it('Should get private content if JWT token is provided and it is valid', function () {
    const response = chakram.get(apiUrl + '/authentication/private', {headers: {'Authorization': 'Bearer ' + token}})
    expect(response).to.have.status(200)
    expect(response).to.have.json('message', 'You can see this with a token.')
    return chakram.wait()
  })
})
