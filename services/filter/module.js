// Filter Module
//

const debug = require('debug')('yabaas:filter')

const jsonFilters = require(process.cwd() + '/json-filters')

// Rule Tree
// route | field | constraints
let rules = jsonFilters.api.filters

// Enforce a rule

exports.filter = function (route) {
  let filter = {}
  filter = rules[route]
  debug('filter: ' + JSON.stringify(filter))
  return filter
}
