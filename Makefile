default: help

.PHONY: help
help:
	@echo "help - Show this help"
	@echo "test - Run all tests"
	@echo "start - Start services"
	@echo "stop - Stop services"
	@echo "build - Include new changes"
	@echo "status - Show services status"


.PHONY: stop
stop:
	docker-compose down --volumes

.PHONY: test
test: stop
test:
	docker-compose -f docker-compose.yml -f docker-compose.test.yml up yabaas_test

.PHONY: start
start:
	docker-compose up -d

.PHONY: build
build:
	docker-compose up -d --build

.PHONY: status
status:
	docker-compose ps
